### 2 Desarrollo de API ###
Si alguna vez has utilizado la API de Facebook, sabrás lo común que "deprecaban" su API. Según usted:

* Explique la razón por la cuál esta no es una buena práctica.
Porque muchos de los desarrollos externos que utilizan la API quedarán inutilizables y
deberán ser refactorizados para poder seguir operando.

* ¿Por qué se dice que las interfaces deben ser "tontas"?
Porque la interfaz no debiera intentar aplicar mecanismos de control, sólo recibir la solicitud y pasarla.
La lógica se debe implementar "aguas arriba".

* Como aplicaria usted los conceptos de Encapsulamiento, Flexibilidad, Seguridad y Versionamiento en el desarrollo de una Interfaz?
No estoy seguro, pero imagino que debo generar interfaces con números de versión, de manera que quienes usan esa interfaz
sepan si hay cambios o tal vez nuevos métodos disponibles.
También usaría encapsulamiento, de manera de no permitir acceso a variables privadas en interfaces públicas, y seguridad 
no permitir guardar cambios o acceso a ciertos métodos si el cliente no se autentifica, por ejemplo.
