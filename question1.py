# Import libraries
from itertools import islice
from itertools import cycle

# Global definitions
hours = range(24)
minutes = range(60)

def reloj(hora,minutos):
    if hora < 0:
        hours.reverse()
        hora = abs(hora)
    if minutos < 0:
        minutes.reverse()
        minutos = abs(minutos)

    def suma(sumaMinutos):
        minutos = minutos + sumaMinutos

    hour = islice(cycle(hours),hora,None)
    minute = islice(cycle(minutes),minutos,None)
    time = str(next(hour)).zfill(2)+':'+str(next(minute)).zfill(2)

    return time
