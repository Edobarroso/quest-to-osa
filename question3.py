### 3 Comprensión ###

Con el siguiente código en python:


    # -*- coding: utf-8 -*-
    def z(c):
        if c[-1] == '?':
    	    return 3
    	else:
    	    return 2

    def y(b):
        r = [l.isupper() for l in b[:-1] if l.isalpha()]

        if r and reduce(lambda a,x : a and x, r): # !
            return 4
        else:
            return z(b)


    def x(a):
        a = ''.join(a.strip().split(' '))
        return y(a) if a else 1


Llamadas:

* x('')
* x('1 2 3 4')
* x('1 2 3 4 A?')
* x('hola?')
* x('HOLA')


Explique según usted:

* ¿Qué hace este código?

La función x(a) elimina todos los espacios de los elementos que se le entregan y los pasa a la función y, siempre y cuando no esté vacío.

La función y(b) toma la salida de x(a) y genera una lista con todas las letras menos la última, guardando true si es mayúscula o false si es minúscula.
Luego, si la lista no es vacía y todas las letras son mayúsculas, pasa los elementos a la función z. En caso contrario, devuelve 4.

La función z(c) devuelve 3 si el último elemento de la lista que ingresamos al comienzo es '?', o 2 si no lo es.

En resumen, la función devolverá:
    - 1 si la lista está vacía
    - 2 si todas las letras de la lista son mayúsculas y no terminan en ?
    - 3 si todas las letras de la lista son mayúsculas y terminan en ?
    - 4 si alguna de las letras de la lista es minúscula

* ¿En la lí­nea marcada con un # !: ¿Qué problema se puede presentar? ¿cómo se soluciona?
Si sólo hay una letra, independiente si es mayúscula o no, el resultado será siempre 4, puesto que el elemento 'x' no existirá.
Falta agregar una condición para este caso de borde, donde se evalúe en forma independiente en caso de haber sólo 1 letra.

* ¿Qué mejora harí­a usted en la implementación del código?
Faltan comentarios!
Fuera de eso, no sé si se justifica separar en tantas funciones diferentes, considerando que todas tienen un objetivo común.
Trataría de dejar junto todo lo que está relacionado, como las funciones y y z, y quizás dejar sólo a x, por hacer las veces de "interfaz".

Si es parte del objetivo, quizás sería necesario implementar encapsulamiento, puesto que es posible acceder a las variables de cualquiera de las funciones en forma libre.
